pi;

function increment = F( t_k, h_k, y_k_minus_1, A, b, c)
	s = numel(b);
	k = zeros(s, 2); # will contain values of lotka evaluated at quadrature points
	
	increment = y_k_minus_1;
	for i = 1:s # i ranges in rk steps

		g_i = y_k_minus_1;
		for j=1:(i-1)
		   g_i = g_i + h_k * A(i, j) * k(j)'
		end

		quadrature_point_i = t_k + c(i) * h_k;
		f_val = lotka(g_i, quadrature_point_i)'
		k(i, :) = f_val;
		increment = increment + f_val * b(i);
	end
endfunction

function y = rungeKutta(initialvalue, T, A, b, c)

	# solves ODE using arbitrary general ERK method 
	# f(x,t), the RHS of our ODE; x is vector, t scalar
	# T is vector containing strictly increasing time points
	# y is vector containing soln, i.e. y_i ~= u_i

	# Butcher Tableau:
	# A is s x s lower triangular matrix
	# b, c are s dim. vectors

	t_k = 0;
	y = initialvalue;

	for k = 1:numel(T) # k ranges in time

	  # calc new interval
	  h_k = T(k) - t_k;
	  t_k = T(k);

	  # fill in general form of explicit 1-step:
	  y = y + h_k * F(t_k, h_k, y, A, b, c)
	  plot(y(: ,1) , y(:, 2)) ;
	  pause(0.01);
	end

	pause(5);
endfunction


function xdot = lotka ( x , t )
  # courtesy of ex1 soln hint
  alfa = 1;
  beta = 1;
  gama = 1;
  dlta = 1;

  xdot = zeros ( 2 , 1 ) ;
  xdot(1) = alfa * x(1) - beta * x(1) * x(2);
  xdot(2) = gama * x(1) * x(2) - dlta * x(2);

endfunction


function y = main(initialvalue, T, A, b, c)
 	# ^rename "main" to "sheet04" later 
	# and delete sheet04 function TODO for executability

	# use rungeKutta to solve LV eqn with parameters
	# alpha = beta = delta = gamma = 1

	figure
	hold on

	for i = 1:11
	  y = rungeKutta([ 0.5 * i, 1 ], T, A, b, c);
	end

	xlabel( " x_1 " ) ;
	ylabel( " x_2 " ) ;
	pause(5);

endfunction

function sheet04
  # TESTING FUNCTION ONLY DELETE ME

  T = linspace (0 , 300 , 9000);
  y_0 = [0.5, 1];

  for num_steps = 5:5

    # modified Euler of order 2:
    A = [0, 0; 0.5,  0];
    c = [0, 0.5];
    b = [0, 1];

    # Heun meth of order 2:
    # A = [0,0; 1,0]
    # c = [0, 1];
    # b = [0.5, 0.5];
    
    y = main(y_0, T, A, b, c);
  end
endfunction

sheet04
