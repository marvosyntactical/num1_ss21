pi = 3.14159265;

function sheet02
  table = ex_b ( 10 ) # lines without trailing ";" get printed
endfunction

function table = ex_b ( maxk )

  # define the following for every k:

  u_0 = 1;

  t_0 = 0;
  t_1 = 2;

  # construct table that will hold values:
  table = zeros( maxk, 5);

  # remember previous two approximations for alpha calculation:
  y_h_km1_N_km1 = 1; # initialization correct? 
  y_h_km2_N_km2 = 1; # initialization correct? 

  # fill table row by row
  for k = 1:maxk

    h_k = 2 .^ (-k);
    N_k = t_1 / h_k; 
    
    # calc euler approximation:
    y_h_k_N_k = explicitEuler(t_0, u_0, h_k, N_k);
    
    # calculate alpha using 2.3c) with a=y:
    diff = y_h_k_N_k - y_h_km1_N_km1;
    ratio_to_prev_diff = diff / (y_h_km1_N_km1 - y_h_km2_N_km2);
    alpha_k = (1/log(2)) * log(abs(ratio_to_prev_diff));

    # fill this row of the table:

    table(k, 1) = k;
    table(k, 2) = h_k;
    table(k, 3) = y_h_k_N_k;
    table(k, 4) = diff;
    table(k, 5) = alpha_k;
    
    # update 'previous' approximations:
    y_h_km2_N_km2 = y_h_km1_N_km1;
    y_h_km1_N_km1 = y_h_k_N_k;
  end
endfunction

# had to change interface to not pass function as arg (throws error before call execution)
function y_h = explicitEuler ( t_0 , u_0 , h , N )
  y_h = u_0; # initialize
  for n = 1:N
    y_h = y_h + h * f(y_h); # calc one step
  end
endfunction


function z = f ( y )
  tau_fourths = pi/2;
  cosine_arg = tau_fourths * y;
  add = cos(cosine_arg);
  subtract = 2*y;

  z = add - subtract;
endfunction


sheet02
