A = [ -21, 19, -20; 19, -21, 20; 40, -40, -40];

function xdot = lorenz(x, t)
  beta = 1000; % beta should be strictly greater 2; when beta close to 2, stiffness ratio is small, 
  % when beta is big, stiffness ratio is big
  sigma = 0; % keep fixed, found thru experimentation on wolfram
  rho = 0; % this value does not matter
  LorenzA = [
    0, sigma, -sigma;
    rho, -1, -x(1);
    x(2), 0, -beta;
  ];
  % x is column 3-vector, as is xdot	
  xdot = A * x
endfunction

function sheet06_ex2

  % Find x0, y0, z0 such that stiffness ratio in t=0 is large/smol

  % This is also an eigenvector of the matrix; corresponding to the eigenvalue -1:

  x0 = 0; % arbitrary
  y0 = 1; % keep fixed, found thru experimentation on wolfram
  z0 = 0; % arbitrary

  initialValue = [x0; y0; z0];
  T = linspace(0, 10, 100);

  y_ = implicitEuler(@lorenz, initialValue, T);

  % plot
  hf = figure();
  plot(y_(:,1), y_(:,2), y_(:, 3))
  xlabel(" x ");
  ylabel(" y ");
  zlabel(" z ");
  s = mat2str(initialValue);
  title(strcat("lorenz IVP with initialValue=", s));  
  print(hf, "plot_prob6_2.png", "-dpng");
  pause(3)

endfunction

% Solution provided by Alejandra
function sheet06_ex4

  % time points
  T = linspace(0, 10, 100);
  
  initialValue = [1;0;-1];    
  experiment(initialValue, T);

endfunction

function experiment(initialValue, T)
  N = length(T);
  
  % GRK for stiff IVP
  y1_ = implicitEuler(@stiffIVP, initialValue, T);
  y2_ = explicitEuler(@stiffIVP, initialValue, T);
  
  % display approximation at last timesteps
  disp(y1_(:, N))   
  disp(y2_(:, N))   
  
  % plot
  hf = figure();
  plot(y1_(:,1), y1_(:,2))
  plot(y2_(:,1), y2_(:,2))
  xlabel(" u ");
  ylabel(" v ");
  s = mat2str(initialValue);
  title(strcat("stiff IVP using implicit vs. explicit Euler, initialValue=", s));  
  print(hf, "plot_prob6_4.png", "-dpng");
  pause(3)
endfunction
  
function y_ = implicitEuler(f, initialValue, T)
  % Problem 6.4.a)	
  d = size(initialValue, 1);
  N = length(T);
  
  y_ = zeros(d, N);
  y_(:,1) = initialValue;

  h0 = T(1);

  % NOTE: figure out how to get the same matrix A from some global namespace into both this function
  % and into stiffIVP without passing it as arg there
  A = [ -21, 19, -20; 19, -21, 20; 40, -40, -40];
  inverted = inv(eye(size(A, 1)) - h0*A)

  for n=2:N
    % assuming all h are the same

    % solving for y
    y_(:, n) = inverted * y_(:, n-1); 
  end
endfunction

function y_ = explicitEuler(f, initialValue, T)
  % Problem 6.4.a)	
  d = size(initialValue, 1);
  N = length(T);
  
  y_ = zeros(d, N);
  y_(:,1) = initialValue;

  for n=2:N
    hn = T(n) - T(n-1);

    F = f(y_(:, n-1), T(n-1));

    size(y_)
    size(F)
    
    % solving for y
    y_(:, n) = y_(:, n-1) + hn * F;
  end
endfunction

function ydot = stiffIVP(y, t)
  % y is column 3-vector, as is ydot	
  A = [ -21, 19, -20; 19, -21, 20; 40, -40, -40];
  ydot = A * y
endfunction

function u = gold(t)
  u = zeros(3,1);
  u(1) = (1/2)*exp(-2*t)+(1/2)*exp(-40*t)*(cos(40*t)+sin(40*t));
  u(2) = (1/2)*exp(-2*t)+(1/2)*exp(-40*t)*(cos(40*t)+sin(40*t));
  u(3) = -exp(-40*t)*(cos(40*t)-sin(40*t));
endfunction

function sheet06
  sheet06_ex2
  sheet06_ex4
endfunction

sheet06


