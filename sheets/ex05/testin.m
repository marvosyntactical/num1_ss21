% Solution to ex04.4 provided by Alejandra
pi;

% lotka-volterra equations
function ydot = lotka( y , t )
  a = 1.0;
  b = 1.0;
  c = 1.0;
  d = 1.0;
  ydot = zeros(1, 2) ;
  ydot(1,1) = a * y(1,1) - b * y(1,1) * y(1,2) ;
  ydot(1,2) = c * y(1,1) * y(1,2) - d * y(1,2) ;
endfunction


function [h_opt, rejected] = adaptiveSSC_computation(yk, ytildek, hk, epsilon, p)
  stability_eps = 0.00001; % avoid divide by zero

  local_error = vecnorm(yk-ytildek)

  if (local_error > epsilon)
    rejected = 1;
  else
    rejected = 0;
  endif

  h_opt = hk * realpow(epsilon/(local_error), 1/(p+1));
endfunction


% explicit runge-kutta method
function [Tcal, y] = embeddedRungeKutta(f, u0, I, A, b, btilde, c, h0, p, epsilon)

  d = size(u0, 2); % dimensionality of y
  s = size(btilde); % num stages

  % initial values
  yk = u0;
  ytildek = u0;

  % time
  T = I(2);
  tk = I(1);
  hk = h0;

  % since we dont know number of timesteps; we would
  % ideally like to have an extensible data structure
  % such as python list. as this doesnt seem to exist
  % in octave, we use zero arrays with a large enough
  % length and cut off later:

  tentativeN = int8((T-tk)/(2*h0));

  % sol^n
  ytilde_ = zeros(tentativeN, d);
  ytilde_(1, :) = u0;
  
  Tcal = zeros(tentativeN);
  Tcal(1) = tk;

  const = 1.1;

  ending = 0;
  % steps:
  k = 2;
  while 1
    % commence 2.4.2.1

    yk = ytilde_(k-1, :);

    % approximations
    ki_ = zeros(s, d);
    ki_(1,:) = f(yk, tk);

    % stages:
    for i=2:s
      t = tk + hk * c(i);
      gi = yk;
      for j=1:i-1
        gi += hk * A(i,j) * ki_(i-1, :);
      end
      ki_(i,:) = f(gi, t);
    end
    
    % quadrature
    F = 0;
    Ftilde = 0;
    for i=1:s
      F += b(i) * ki_(i, :);
      Ftilde += btilde(i) * ki_(i, :);
    end
    
    % save ERK update step
    y = yk + hk * F
    ytilde = yk + hk * Ftilde

    % pow = realpow(2,p);
    % ytilde_(k,:) = (pow*ytilde - y)/(pow-1);
    ytilde_(k,:) = ytilde;

    if ending
      % Finish procedure
      ytilde_ = ytilde_(1:k,:);
      Tcal = Tcal(1:k);
      break;
    endif

    % Adaptive Step Size Control from here on: 

    [h_opt, rejected] = adaptiveSSC_computation(y, ytilde, hk, epsilon, p);

    % redo computation and increment everything 
    if rejected
      % 2.4.2.3:
      % dont increment k (redo same computations with hk = h_opt)
      disp(strcat("rejected hk=", num2str(hk)));
      hk = h_opt;

    else
      % 2.4.2.4:
      disp(strcat("accepted hk=", num2str(hk)));

      % Remark 2.4.3:
      remaining_time = T-tk;

      if (remaining_time <= const * h_opt)
	hk = remaining_time;
        ending = 1;
      else
	hk = h_opt;
      endif
      
      k = k + 1;
      tk = tk + hk;
      Tcal(k) = tk;
    endif

    disp(strcat("nxt w/ h_opt=", num2str(hk)));
    
  endwhile
endfunction


function [Tcal, y] = sheet05_tobias_vormbruck_marvin_koss(f, I, u0, epsilon)

  % embedded BT coeffs (for ode45)


  A = [
             0,             0,             0,             0,          0,      0,      0;
             0,             0,             0,             0,          0,      0,      0;
          3/40,          9/40,             0,             0,          0,      0,      0;
         44/45,        -56/15,          32/9,             0,          0,      0,      0;
    19372/6561,   -25360/2187,    64448/6561,      -212/729,          0,      0,      0;
     9017/3168,       -355/33,    46732/5247,        49/176,-5103/18656,      0,      0;
        35/384,             0,      500/1113,       125/192, -2187/6784,  11/84,      0;
  ]
  c = [   0, 1/5, 3/10,  4/5,  8/9,    1,    1,  ]

  btilde = [     35/384,          0,      500/1113,    125/192,      -2187/6784,       11/84,         0]
  b      = [ 5179/57600,          0,    7571/16695,    393/640,   -92097/339200,    187/2100,      1/40]

  p = 4;
  initial_h = 0.1;
  
  % erk for lotka-volterra equation
  [Tcal, y] = embeddedRungeKutta(f, u0, I, A, b, btilde, c, initial_h, p, epsilon);
 endfunction


function testin
  % some testing code

  % time interval
  t0 = 0;
  T = 30;
  I = [t0, t0+T];

  % maximally tolerated local error (approxilatatered by y-ytilde)
  epsilon = 0.02;
  
  hf = figure();
  hold on

  % number of initial values to test 
  j = 1;
  
  for i = 1:j
  
    u0 = [0.5 * i, 1];    
    [Tcal, y] = sheet05_tobias_vormbruck_marvin_koss(@lotka, I, u0, epsilon)

    % display approximation at last timestep
    % disp(y)   
    
    % plot
    plot(y(:,1), y(:,2))
    s = mat2str(u0);
    title(strcat("Lotka-Volterra Equations using embedded ERK, initialValue=", s));  
    print(hf, "plot_prob5_3.png", "-dpng");
    xlabel(" u ");
    ylabel(" v ");
    pause(3);

  end
endfunction


testin

% ROADMAP

% 1. implement embeddedRungeKutta
% 2. implement adpative time step control

