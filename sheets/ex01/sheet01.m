function sheet01
  t = linspace (0 ,300 ,3000);
  figure
  hold on
  for i = 1:11
    x = lsode ("lotka" , [ 0.5 * i , 1 ] , t ) ;
    plot (x (: ,1) , x ( : , 2 ) ) ;
  end
  xlabel ( " x_1 " ) ;
  ylabel ( " x_2 " ) ;
endfunction

function xdot = lotka ( x , t )
  a = 1;
  b = 1;
  c = 1;
  d = 1;
  xdot = zeros ( 2 , 1 ) ;
  xdot ( 1 ) = a*x(1) -b*x ( 1 ) * x ( 2 ) ;
  xdot ( 2 ) = c *x ( 1 ) * x(2) -d*x ( 2 ) ;
endfunction

sheet01
